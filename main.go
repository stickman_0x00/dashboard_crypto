package main

import (
	"fmt"
	"time"

	"gitlab.com/stickman_0x00/dashboard_crypto/coingecko"
	"gitlab.com/stickman_0x00/dashboard_crypto/dashboard"
)

func main() {
	config, err := NewConfig(config_path)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Prepare structure
	prices := make(coingecko.Simple_prices)
	// Loop each currency map
	for _, currency := range config.Currencies {
		// Get currency name + coins of currency
		for currency_name, coins := range currency {
			for _, coin := range coins {
				prices[coin+"_"+currency_name] = coingecko.Simple_price{}
			}
		}
	}

	// Create dashboard with number of lines needed
	d := dashboard.New(len(prices))

	// Infinite loop, refresh dashboard and prices
	for {
		// Loop each currency map
		for _, currency := range config.Currencies {
			// Get currency name + coins of currency
			for currency_name, coins := range currency {
				// Request values for each currency + coins
				results := coingecko.Request_simple_price(currency_name, coins...)
				for name, price := range results {
					prices[name] = price
				}
			}
		}
		go d.Print(prices)
		time.Sleep(time.Minute / 6)
	}

}
