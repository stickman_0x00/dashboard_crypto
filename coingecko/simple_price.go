package coingecko

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

const simple_price_url = "/simple/price?ids=%s&vs_currencies=%s&include_24hr_change=true&include_last_updated_at=true"

type Simple_price struct {
	Price     float64
	Change_24 float64
}

type Simple_prices map[string]Simple_price

func Request_simple_price(currency string, coin_names ...string) Simple_prices {
	// build url
	url := fmt.Sprintf(base_url+simple_price_url, url.QueryEscape(strings.Join(coin_names, ",")), currency)
	// fmt.Println("url: ", url)

	// request
	resp, err := http.Get(url)
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	// Check if request successed
	if resp.StatusCode != 200 {
		fmt.Println("Too many requests :(")
		return nil
	}

	// Json to general map[string]interface{}
	var json_values map[string]interface{}
	if err = json.NewDecoder(resp.Body).Decode(&json_values); err != nil {
		return nil
	}

	return simple_price_convertion(currency, json_values)
}

// Conversion of json to structure
func simple_price_convertion(currency string, json_values map[string]interface{}) Simple_prices {
	// Prepare variables for convertion
	prices := make(Simple_prices)
	field_change := currency + "_24h_change"

	// Loop response of each coin
	for coin, value := range json_values {
		conversion_name := coin + "_" + currency
		// Convert interface to map[string]interface{}
		if fields, ok := value.(map[string]interface{}); ok {
			// Convert to structure
			prices[conversion_name] = Simple_price{
				Price:     fields[currency].(float64),
				Change_24: fields[field_change].(float64),
			}
		}
	}

	return prices
}
