# Dashboard Crypto

Show prices and 24h changes of a specific coin in any currency (currencies supported by Coingecko).

# Here's a sample video:

![Sample Video](demo.mp4)