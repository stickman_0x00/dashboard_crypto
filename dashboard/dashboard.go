package dashboard

import (
	"fmt"
	"sort"
	"time"

	"gitlab.com/stickman_0x00/dashboard_crypto/coingecko"
)

const cursor_top = "\033[H"
const clear = "\033[2J"
const reset_color = "\033[0m"
const color_green = "\033[92m"
const color_red = "\033[91m"
const color_yellow = "\033[33m"

var fields_names = []interface{}{
	"Coin",
	"Price",
	"24h",
}

type Dashboard struct {
	lines      int
	old_prices coingecko.Simple_prices
}

func New(lines int) Dashboard {
	// Move cursor to the top of screen, clear screen
	fmt.Print(cursor_top, clear)

	return Dashboard{
		lines:      lines,
		old_prices: make(coingecko.Simple_prices),
	}
}

func title() {
	fmt.Printf("%-20s\t%10s\t%6s", fields_names...)
}

func (me *Dashboard) Print(prices coingecko.Simple_prices) {
	// Move cursor to the top of screen
	fmt.Print(cursor_top)

	title()

	// Sort by 24 change
	// We are expecting to never get the same 24h
	keys_names := make(map[float64]string) // Have the corresponding names of the float
	keys := make([]float64, 0, me.lines)   // Save the sorted floats
	for name, price := range prices {
		keys_names[price.Change_24] = name   // Join name and float together
		keys = append(keys, price.Change_24) // Get floats to order
	}

	// Order floats
	sort.Sort(sort.Reverse(sort.Float64Slice(keys)))

	me.print_coins(keys, keys_names, prices)

	// Copy of price
	for name, price := range prices {
		me.old_prices[name] = price
	}

	// Remove yellow (value changed)
	time.Sleep(time.Second * 2)
	me.print_coins(keys, keys_names, prices)
}

func (me Dashboard) print_coins(keys []float64, keys_names map[float64]string, prices coingecko.Simple_prices) {
	fmt.Print(cursor_top)
	// Loop floats
	for _, key := range keys {
		// Get corresponding name of the float
		name := keys_names[key]

		print_coin(name, prices[name].Price, prices[name].Change_24, me.old_prices[name].Price != prices[name].Price)
	}
}

func print_coin(name string, price, change_24 float64, change bool) {
	fmt.Printf("\n%-20s\t", name)

	// In case the value changed print yellow
	if change {
		fmt.Print(color_yellow)
	}
	fmt.Printf("%10g\t", price)

	// Color %
	if change_24 > 0 {
		fmt.Print(color_green)
	} else {
		fmt.Print(color_red)
	}
	fmt.Printf("%5.1f%%", change_24)

	// Reset for next input
	fmt.Print(reset_color)
}
